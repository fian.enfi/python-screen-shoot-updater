from tkinter import Tk, messagebox
# from tkinter import ttk
from tkinter.ttk import Progressbar, Label
from data import save_var
from utils import ApiClass as ac
# import json
from json import dump, load
import os
from requests import get
import sys
from threading import Thread, Event
from zipfile import ZipFile

if sys.platform == 'darwin':
    application_path = ""
else:
    if getattr(sys, 'frozen', False):
        application_path = os.path.dirname(sys.executable)
    elif __file__:
        application_path = os.path.dirname(__file__)
        application_path = os.path.join(application_path, os.pardir)
    else:
        application_path = ""
print("App Path", application_path)
app = Tk()
app.title("Update - SWC Intelligent")
app.iconbitmap("img/swc.ico")
# logger = Logger.Logger("/log")
w = 550  # width for the Tk root
h = 100  # height for the Tk root

# get screen width and height
screen_width = app.winfo_screenwidth()
screen_height = app.winfo_screenheight()

ws = screen_width  # width of the screen
hs = screen_height  # height of the screen

# calculate x and y coordinates for the Tk root window
x = (ws/2) - (w/2)
y = (hs/2) - (h/2)

# set the dimensions of the screen
# and where it is placed
app.geometry('%dx%d+%d+%d' % (w, h, x, y))
# app.geometry(f"{screen_width}x{screen_height - 150}+0+0")
# app.wm_geometry("+4000+4000")
# app.after(500, lambda: app.wm_geometry("+0+0"))
app.configure(background='#F9F9F9')

app.call('wm', 'attributes', '.', '-topmost', '1')
# app = ttk.Window()
app.resizable(0, 0)
# app.overrideredirect(True)
ZIP_URL = "https://www.swc-dent.com/main/public/front/app/Intelligent%20Report.zip"


def save_data():
    print("saving data")
    apiObj = ac.ApiClass()
    apiObj.get_client_token()
    response = apiObj.get_version()
    if response:
        with open("data/save_var.json", 'r+') as f:
            data = load(f)
            data["version"] = response["latest_version"]
            # <--- should reset file position to the beginning.
            f.seek(0)
            dump(data, f, indent=2)
            f.truncate()


def download_file():
    url = "https://www.swc-dent.com/main/public/front/app/Intelligent%20Report%20.exe"
    # local_filename = url.split('/')[-1]
    local_filename = f'Intelligent Report.exe'
    # NOTE the stream=True parameter below
    print("request download")
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
    }
    while True:
        if stop_event.is_set():
            break
        with get(url, headers=headers, stream=True, allow_redirects=True) as r:
            r.raise_for_status()
            print("ready to download")
            os.remove(local_filename)
            total_length = int(r.headers.get('content-length'))
            print(total_length)
            with open(local_filename, 'wb') as f:
                chunk_size = 0
                print("start download")
                for chunk in r.iter_content(chunk_size=8192):
                    # If you have chunk encoded response uncomment if
                    # and set chunk_size parameter to None.
                    # if chunk:
                    chunk_size += 8192
                    decimal = chunk_size/total_length
                    persen = int(decimal * 100)
                    my_label.config(text=f"{persen}%")
                    my_progress['value'] = persen
                    print(chunk_size)
                    f.write(chunk)
            save_data()
            print("done")
            break
    os.startfile(local_filename)
    app.destroy()
    os.remove(sys.argv[0])
    sys.exit()


def stop_update():
    stop_event.set()
    app.quit()


def download_and_save(url):
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
    }
    # logger.log("Download Started")
    while True:
        if stop_event.is_set():
            # logger.log("Download Stopped")
            break
        try:
            with get(url, headers=headers, stream=True, allow_redirects=True) as req:
                req.raise_for_status()
                total_length = int(req.headers.get('content-length'))
                # logger.log("total size " + str(total_length))
                chunk_size = 0
                print("start download")
                with open(f"new_version_download", "wb") as zip:
                    for chunk in req.iter_content(chunk_size=8192):
                        # If you have chunk encoded response uncomment if
                        # and set chunk_size parameter to None.
                        # if chunk:
                        chunk_size += 8192
                        decimal = chunk_size/total_length
                        persen = int(decimal * 100)
                        my_label.config(text=f"Downloading: {persen}%")
                        my_progress['value'] = persen
                        zip.write(chunk)
                save_data()
                print("download complete")
                break
        except Exception as e:
            messagebox.showerror("Connection Error!",
                                 f"Check your connection and try again.\n\nException: {e}")
            app.destroy()
            sys.exit()
    # logger.log("download completed")
    try:
        os.rename("new_version_download", "new_version_download.zip")
    except:
        messagebox.showerror(
            "Error!", "The downloaded file can't be found.")

    try:
        os.rename(f"updater.exe", "updater_old.exe")
    except Exception as e:
        print("rename updater", e)
        messagebox.showerror(
            "Error!", "Please do not change any application name.\nThe updater.exe is not found.")

    try:
        with ZipFile(f"new_version_download.zip") as zip:
            # logger.log("Extracting..")
            zip.extractall()
    except Exception as e:
        print("extractall", e)
        messagebox.showerror(
            "Error!", f"Error when extracted the zip\n\nException: {e}")

    # logger.log("Done extracting")
    os.remove("new_version_download.zip")
    os.startfile("Intelligent Report.exe")
    app.destroy()
    os.remove(sys.argv[0])
    sys.exit()


with open("data/save_var.json") as outfile:
    data = load(outfile)
save_var.token = data["token"]
save_var.name = data["name"]
save_var.email = data["email"]
save_var.remember_me = data["remember_me"]
save_var.screen_width = data["screen_width"]
save_var.screen_height = data["screen_height"]
save_var.folder_id = data["folder_id"]
save_var.patient_name = data["patient_name"]
save_var.usr = data["usr"]
save_var.pwd = data["pwd"]
save_var.mode = data["mode"]
save_var.version = data["version"]

my_progress = Progressbar(
    app, orient="horizontal", mode="determinate", length=500)
my_progress.pack(pady=20)
my_label = Label(app, text="0%", padding=5, background='#F9F9F9')
my_label.pack()

stop_event = Event()
thread = Thread(target=download_and_save, args=(ZIP_URL,))
thread.daemon = True
thread.start()

app.protocol("WM_DELETE_WINDOW", stop_update)
# logger.log("Windows starting")
app.mainloop()

# build pyinstaller -F --onefile --icon=img/swc.ico updater.py --uac-admin
