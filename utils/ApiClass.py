import requests
from data import save_var


class ApiClass:
    def __init__(self):
        if save_var.mode == "conv-dev":
            self.base_token_url = "https://api.asgardeo.io/t/smilewithconfidence/oauth2/token"
            self.base_url = "http://162.240.6.124:9054/swc-ai-tools/"
        elif save_var.mode == "prod":
            self.base_token_url = "https://api.asgardeo.io/t/smilewithconfidence/oauth2/token"
            self.base_url = "https://09b85129-5378-499f-8f69-e84e47018bdb-prod.e1-eu-north-azure.choreoapis.dev/cvlj/swcaiapi/v1.0/swc-ai-tools/"

    def postData(self, url, data, headers=None):
        # print("apiClass postData:"+url,data,headers)
        try:
            return requests.post(url, data=data, headers=headers)
        except requests.exceptions.RequestException as e:
            return ""
            # return json.dumps({"code": "502", "message": e}, indent=4)

    def get_client_token(self):
        print("apiClass get_client_token")
        url = self.base_token_url
        data = {'grant_type': 'client_credentials'}
        header = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ZW1OQUhYOFJSS2Z1SW5kX21RYXl4cEVPSjhrYTp3Tm1pSzluR1AyVndoVHFNYmNpZHpid2pNUU44cmFzcWFlTlhFRXNlQXQ4YQ=='
        }
        response = self.postData(url, data, header)
        save_var.api_client_token = response.json()["access_token"]
        return response.json()["access_token"]
        # print("client_token:",save_var.api_client_token)

    def getData(self, url, data=None, headers=None):
        print("apiClass getData:"+url)
        try:
            return requests.get(url, params=data, headers=headers)
        except requests.exceptions.RequestException as e:
            return ""

    def get_version(self):
        print("apiClass get version")
        url = self.base_url + "1.0.0/version"
        auth_token = self.get_client_token()
        header = {
            'Authorization': f"Bearer {auth_token}"
        }
        response = self.getData(url, headers=header)
        if response:
            return response.json()
        return {'code': '99', 'message': 'Cannot get response'}
