# Installation Instruction

## Install Without Console
```
pyinstaller -F --onefile --icon=img/swc.ico -w updater.py --uac-admin
```

### Note
Only install necessary package / library. 
Required package / library listed in requirements.txt